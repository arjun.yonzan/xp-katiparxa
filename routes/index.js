var express = require('express');
var router = express.Router();
const fs = require('fs')
var elasticsearch = require('elasticsearch');
const slug = require('slug')

const Model = require('../model')
const ENV = require('../environment')

const stripHtml = require("string-strip-html");
var {google} = require("googleapis");
var request = require('request');

var key = require("./service_account.json");



/*FOR GOOGLE APIS*/
const jwtClient = new google.auth.JWT(
  key.client_email,
  null,
  key.private_key,
  ["https://www.googleapis.com/auth/indexing"],
  null
);


/*----ELASTIC SEARCH SETUP----*/
const bonsai_url = 'https://i7gj5j3vjq:u79ix1eg6r@langtang-4948254199.us-west-2.bonsaisearch.net:443'
const elasticClient = new elasticsearch.Client({
	host: bonsai_url,
	// log: 'trace'
});

elasticClient.ping({
	// ping usually has a 3000ms timeout
	requestTimeout: 3000
}, function(error) {
	if (error) {
		console.trace('elasticsearch cluster is down!');
	} else {
		console.log('Elastic All is well');
	}
});


const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "katiparxa"
});

con.connect(function(err) {
  if (err) throw err
  console.log("MySQL OK", "$$$$");
});


//file upload
/*var multer = require('multer');
var upload = multer({
	dest: 'uploads/'
});*/

/*ELASTIC SEARCH*/
router.get("/search", async(req,res,next)=>{
	const q = req.query.q
	console.log(q, "q--------$$$$");
	const response = await elasticClient.search({
		index: 'katiparxa',
		size:20,
		q:q
	})
	res.json(response.hits.hits);
})

//create update elastic index
router.post("/elasticsearch/index", async (req, res, next) => {
	console.log("indexing....", "$$$$");
	//security validation later.......@todo
	let data = req.body
	try {
		const response = await elasticClient.index({
			index: 'katiparxa',
			id: data.id,
			body: {
				slug: data.slug,
				title: data.title,
				price: data.price,
				brand: data.brand,
				categories: data.categories,
				images: data.images,
				content: stripHtml(data.content),
				published: data.published ? true : false
			}
		})

		console.log(response, "$$$$");
		res.json({success:true, result: response})
	} catch (err) {
		console.log(err, "$$$$");
		res.status(400).json({err, success:false})
	}
})

router.get("/elasticsearch/index_all", async (req, res, next) => {

	let sql = `SELECT p.id, p.title, p.price, p.description as content, p.slug, p.published, b.name as brand, sc.name as categories, m.filename as images from products as p 
			left join brands as b on b.id = p.brand_id
			left join subcategories as sc on sc.id = p.category_id
			left join product_image as pi on p.id = pi.product_id 
			left join medias as m on pi.media_id = m.id 
			order by p.id desc `

	con.query(sql, async function(err, result, fields) {
		if (err) throw err;
		let products = result

		console.log(products.length, "$$$$");

		const productsBody = products.flatMap(doc => [{ index: { _index: 'katiparxa', _id:doc.id } }, {...doc,published: doc.published ===1 ? true:false, content: doc.content?stripHtml(doc.content):"" }])

		const bulkResponse = await elasticClient.bulk({ refresh: true, body:productsBody })

		if (bulkResponse.errors) {
			const erroredDocuments = []
			// The items array has the same order of the dataset we just indexed.
			// The presence of the `error` key indicates that the operation
			// that we did for the document has failed.
			bulkResponse.items.forEach( async (action, i) => {
				const operation = Object.keys(action)[0]
				if (action[operation].error) {
					erroredDocuments.push({
						// If the status is 429 it means that you can retry the document,
						// otherwise it's very likely a mapping error, and you should
						// fix the document before to try it again.
						status: action[operation].status,
						error: action[operation].error,
						operation: productsBody[i * 2],
						document: productsBody[i * 2 + 1]
					})
				}
			})
			console.log(erroredDocuments)
			res.status(400).json(erroredDocuments)
		}

		const count = await elasticClient.count({ index: 'katiparxa' })
		console.log(count, "the count.....")

		res.json(count)
	});
})


//delete elastic index
router.delete("/elasticsearch/doc/:id", async (req,res,next)=>{
	try{
		let id = req.params.id
		let response = await elasticClient.delete({
			index: 'katiparxa',
			id: id.toString()
		})
		console.log(response, "---delete response ---- $$$$");
		res.json({success:true, result:response})
	}catch(err){

		if(err.statusCode===404){
			res.json({success:true, result:"NOT FOUND"})
			return;
		}
		// console.log(err, "$$$$");
		res.status(400).json({err, success:false})
	}
})



function randomNumbers() {
	return Math.floor(1000 + Math.random() * 9000);
}

function striphtml(html) {
	return String(html).replace(/(<([^>]+)>)/ig,"").trim()
}

/*GOOGLE INDEXING API*/
router.post('/googleapis/indexing/', (req,res,next)=>{

	let {body} = req
	let url = body.url;
	let type = body.type;

	if(!url, !type) { res.status(400).json("url or type is needed")}

	jwtClient.authorize(function(err, tokens) {
		if(err){
			console.log(err, "$$$$");
			return;
		}

  	let options = {
	    url: "https://indexing.googleapis.com/v3/urlNotifications:publish",
	    method: "POST",
	    // Your options, which must include the Content-Type and auth headers
	    headers: {
	      "Content-Type": "application/json"
	    },
	    auth: { "bearer": tokens.access_token },
	    // Define contents here. The structure of the content is described in the next step.
	    json: {
	      "url": url,
	      "type": type == "UPDATED"? "URL_UPDATED" : "URL_DELETED" // there is issue error when made dynamic... think of something...
	    }
		}
		request(options, function (err, response, body) {
		  if(err){
		  	console.log(err, "$$$$");
		  	res.status(400).json(err)
		  }
		  console.log(body, "---response----google api---$$$$");
	    res.json(body)
	  });

	});
});

/*==================== ROUTING BEGINS......... =========================*/
router.get('/ping', (req, res, next) => {
	res.json({
		success: true
	})
})

/* GET home page. */
router.get('/', function(req, res, next) {
	res.send("kaitparxa.com back end");
});


//IF you want to USE socket io then here.....
module.exports = (io) => {
	return router;
}