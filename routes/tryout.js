var express = require('express');
var router = express.Router();

const fs = require('fs')
var XMLWriter = require('xml-writer')
const ENV = require('../environment')
const Model = require('../model')


/* GET users listing. */
router.get('/', function(req, res, next) {
	res.send('trying out...');
});

let slug = require('slug')
router.get('/slug', (req, res, next) => {

	Model.Post.find({}, function(err, posts) {
		if (err) {
			throw err
		}
		let results = []
		posts.forEach(function(post, index) {
			let postSlug = slug(post.title, {
				lower: true
			})
			Model.Post.update({
				_id: post._id
			}, {
				slug: postSlug
			}, function(err, result) {
				results.push(postSlug)
				if (index + 1 == posts.length) {
					res.json({
						results
					})
				}
			});
		});
	});
})


router.get('/slug/new', (req, res, next) => {

	function randomNumers() {
		return Math.floor(1000 + Math.random() * 9000);
	}

	let title = "iphone x 2018"
	let postslug = slug(title + " " + randomNumers(), {
		lower: true
	})

	var model = new Model.Post({
		title: title,
		slug: postslug,
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
	});

	model.save(function(err, post) {
		if (err) {}
		res.json({
			post
		});

	});

	return;
})

//sitemap work
router.get('/sitemap', (req, res, next) => {

	xw = new XMLWriter(true);
	xw.startDocument("1.0", 'UTF-8');
	xw.startElement('urlset').writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

	Model.Post.find({}, function(err, posts) {
		if (err) {throw err}
		posts.forEach((post, postIndex) => {
			let loc = 'http://katiparxa.com/posts/'+post.slug
			xw.startElement('url')
				xw.writeElement('loc',loc)
				xw.writeElement('changefreq',"daily")
				xw.writeElement('priority','0.5')
			xw.endElement()
			if(postIndex+1 == posts.length){
				xw.endDocument();
				let path = '/var/www/katiparxa/dist/sitemap.xml'
				// path = "sitemap.xml"
				fs.writeFile(path, xw.toString(), (err) => {
					if (err) throw err;
					res.json({
						totalPosts: posts.length,
						completed: true
					})
				})
			}
		})
	});
})





module.exports = router;