
router.get('/manage/postimages', function(req, res, next) {

	Model.Post.find({}, function(err, results) {
		if (err) {}
		results.forEach((post, postIndex) => {

			console.log(post._id, "-----post-id------$$$$");
			console.log(post.image, "$$$$");


			Model.Post.findOneAndUpdate({
				_id: post._id
			}, {
				$push: {
					images: {
						$each: [post.image]
					}
				}
			}, function(err, result) {
				if (postIndex + 1 == results.length) {
					res.json({
						done: results.length
					})
				}
			})

		})
	});

})


/*const checkJwt = jwt({
	// Dynamically provide a signing key
	// based on the kid in the header and 
	// the signing keys provided by the JWKS endpoint.
	secret: jwksRsa.expressJwtSecret({
		cache: true,
		rateLimit: true,
		jwksRequestsPerMinute: 5,
		jwksUri: `https://katiparxa.auth0.com/.well-known/jwks.json`
	}),

	// Validate the audience and the issuer.
	audience: '9su12QzLU9QVYtbuNF3BFsrfh4GUUlrR',
	issuer: `https://katiparxa.auth0.com/`,
	algorithms: ['RS256']
});
*/




//manage post
console.log("**********************************************", "$$$$");
router.get('/manage/images', (req, res, next) => {

	fs.readdir('public/images', (err, files) => {
		files.forEach((file, index) => {

			Jimp.read('public/images/' + file, (err, image) => {


				if (err) res.status(400).json(err)
				//$$$
				if (image.bitmap.width > 800) {
					image.resize(800, Jimp.AUTO).quality(70).write('public/images/' + file, function(err, file) {

						if (file) {
							if (index + 1 == files.length) {
								console.log("finally......", "$$$$");
								res.json({
									done: true
								})
							}
						}
					})
				} else {
					//
					console.log("else", index, files.length, "$$$$");
					if (index + 1 == files.length) {
						res.json({
							done: true
						})
					}
				}
			})

		})
	})

})



router.get('/manage/users', (req, res, next) => {

	var options = {
		method: 'GET',
		url: 'https://katiparxa.auth0.com/api/v2/users',
		headers: {
			authorization: 'Bearer ' + auth0.token
		}
	};

	request(options, function(error, response, body) {
		if (error) throw new Error(error);

		let users = JSON.parse(body)
		users.forEach((user, userIndex) => {
			var model = new Model.User({
				name: user.name,
				email: user.email,
				picture: user.picture,
				openID: user.user_id
			});

			model.save(function(err) {
				if (err) {
					res.status(400).json(err)
				}

				if (userIndex + 1 == users.length) {
					res.json({
						done: true,
						count: users.length
					});
				}

			});
		})
	});
})



/*==================== .....FOR DEV PURPOSES.... =========================*/
var request = require("request");
const auth0 = require('../auth0')

router.get('/manage', (req, res, next) => {
	var options = {
		method: 'GET',
		url: 'https://katiparxa.auth0.com/api/v2/users',
		headers: {
			authorization: 'Bearer ' + auth0.token
		}
	};

	request(options, function(error, response, body) {
		if (error) throw new Error(error);
		let users = JSON.parse(body)
		res.json(users)
	});
})



//delete image
router.delete('/post/images', (req, res, next) => {
	let image = req.query.img
	let openID = req.user.sub
	Model.Post.findOne({
		images: image
	}, function(err, post) {
		if (post && openID == post.openID) {
			fs.unlink('public/images/' + image, (err) => {
				if (err) {
					res.status(400).json(err)
					return
				}
				Model.Post.update({
					_id: post.id
				}, {
					$pull: {
						images: image
					}
				}, function(err, result) {
					if (err) {
						res.status(400).json(err)
					}
					res.json(result)
				});
			}) //end unlink
		} //end if
	}) //end findOne
})


router.get('/categories', (req, res, next) => {

	Model.Category.find({}, function(err, categories) {

		if (err) {
			res.status(400).json(err)
		}
		res.json(categories)

	});

})



//GET SINGLE POSTS>>>>>>>>>
router.get('/posts/:id', (req, res, next) => {
	let id = req.params.id
	Model.Post
		.findOne({
			_id: id
		})
		.populate('user')
		.exec(function(err, result) {
			if (err) {
				res.status(err.status).json(err)
			}
			res.json(result);
		});
})

router.get('/posts/slug/:slug', (req, res, next) => {

	console.log(req.params, "------$$$$");
	let postSlug = req.params.slug
	Model.Post
		.findOne({
			slug: postSlug
		})
		.populate('user')
		.exec(function(err, result) {
			if (err) {
				res.status(err.status).json(err)
			}
			res.json(result);
		});
})

router.get('/all', (req, res, next) => {
	Model.Post
		.find({})
		.populate('user')
		.sort({
			"updatedAt": -1
		})
		.exec(function(err, result) {
			if (err) {}
			res.json(result)
		});
})

router.post('/no/posts', upload.any(), (req, res, next) => {


	/*
	1. VALIDATIONS ✔?
	2. UPLOAD FILES AND COLLECT FILE NAME ARRAY
		- FILE SIZE CHECK ✗
	3. CREATE A POST MODEL ✔
	4. INDEX ON ELASTICSEARCH ✔
	*/
	/*==================== VALIDATIONS =========================*/
	let post = req.body
	if (!post.title && !post.description) {
		console.log(100, "$$$$");
		res.status(400).json({
			error: "title | description is empty"
		})
		return
	}; /*-- END VALIDATIONS --*/

	//MULTIPLE CASE/////
	if (req.files.length) {
		if (req.files.length > 4) {
			res.status(400).json({
				"error": "no more than 4 images please"
			})
			return;
		}

		let images = []
		req.files.forEach(function(file, fileIndex) {
			let fileName = (new Date).valueOf() + '-' + file.originalname
			fs.rename(file.path, `public/images/${fileName}`, (err) => {
				if (err) {
					res.status(400).json(err)
				}
				images.push(fileName)
				if (fileIndex + 1 == req.files.length) {
					console.log("all images", "$$$$");
					savePost(images)
				}
			})
		});
	} else {
		console.log("ELSE", "$$$$");
		savePost()
	}

	function savePost(images) {
		let profile = req.user
		let openID = profile.sub
		let post = req.body

		Model.User.findOne({
			openID: openID
		}, function(err, user) {
			let postSlug = slug(post.title + " " + randomNumbers(), {
				lower: true
			})
			let data = {
				title: post.title,
				slug: postSlug,
				description: post.description,
				price: post.price,
				brand: post.brand,
				category: post.category,
				vendor: post.vendor,
				images: images || undefined,
				source: post.source,
				openID: openID,
				user: user._id,
				updatedAt: Date.now()
			}

			if (err) {
				throw err
			}

			var postModel = new Model.Post(data);
			postModel.save(function(err, result) {
				if (err) {
					res.status(400).json(err)
				}
				if (result) {
					//ElasticSearch Portion
					data['images'] = result.images || []
					elasticClient.create({
						index: 'katiparxa',
						type: 'posts',
						id: result._id.toString(),
						body: data
					}, (err, response) => {
						if (err) {
							res.status(400).json(err)
						} else {
							res.json(result)
						}
					}) //end elastic create()
				} //end if
			}); //end save()
		}); //end findOne
	} //end fn savePost
}) //end /post

//PUT POSTS=======================================
router.put('/posts/:id', upload.any(), (req, res, next) => {

	/*===
	1. VALIDATIONS ✔
	2. FILES UPLOADING AND NAME COLLECTION ✔
	3. UPDATE POST ✔
	4. UPDATE ELASTIC INDEX ✔ used client.index() //consider .update()
	====*/

	let post = req.body
	if (!post.title && !post.description) {
		res.status(400).json({
			error: "title | description is empty"
		})
		return
	}; /*-- END VALIDATIONS --*/

	let images = []
	if (req.files.length) {
		req.files.forEach(function(file, fileIndex) {
			let fileName = (new Date).valueOf() + '-' + file.originalname
			fs.rename(file.path, `public/images/${fileName}`, (err) => {
				if (err) {
					res.status(400).json(err)
				}
				images.push(fileName)
				if (fileIndex + 1 == req.files.length) {
					updatePost(images)
				}
			})
		});
	} else {
		updatePost()
	}

	function updatePost(images) {
		let postId = req.params.id
		let postSlug = slug(post.title + " " + randomNumbers(), {
				lower: true
			})
		let data = {
			title: post.title,
			slug: postSlug,
			description: post.description,
			price: post.price || undefined,
			brand: post.brand,
			category: post.category,
			vendor: post.vendor,
			source: post.source,
			updatedAt: Date.now()
		}
		Model.Post.findByIdAndUpdate({
			_id: postId
		}, {
			$push: {
				images: {
					$each: images || []
				}
			},
			$set: data
		}, function(err, result) {
			if (err) throw err

			//else ELASTIC OPERATIONS
			data.images = result.images
			elasticClient.index({
				index: 'katiparxa',
				type: 'posts',
				id: String(postId),
				body: data,
			}, function(error, response) {
				if (error) {
					console.log("elastic erro.", "$$$$");
					throw error
					res.status(400).json(err)
					return;
				}
				//if success
				res.json({
					result: {
						node: result,
						elastic: response
					}
				})
			})
		});
	}
})

//DELETE POST
router.delete("/posts/:id", (req, res, next) => {

	//0.CHECK IF USER | ROLE IS ALLOWED ✗
	//1.DELETE FROM ELASTIC ✔
	//2.REMOVE THE IMAGES FILES ✔
	//3.DELETE FROM DATABASE ✔ -----------/
	let id = req.params.id
	elasticClient.delete({
		index: 'katiparxa',
		type: "posts",
		id: id.toString()
	}, function(err, response) {
		//ELASTIC NOT_FOUND
		if (err) {
			if (err.status == 404) {
				console.log("not found in elastic...but deleting...", "$$$$");
				deletePost()
			} else {
				res.status(err.status).json(err)
			}
		}
		//ALL GOOD
		if (response.found && response.result == 'deleted') {
			console.log("all good...deleting...", "$$$$");
			deletePost()
		}
	});

	function deletePost() {
		//removing the images files 
		Model.Post.findOne({
			_id: id
		}, function(err, result) {
			if (err) {
				throw err;
			}
			console.log("REMOVING THE IMAGES.....", "$$$$");
			let images = result.images || []
			if (images.length) {
				images.forEach((img, imgIndex) => {
					let filepath = 'public/images/' + img
					fs.access(filepath, (err) => {
						if (err) {
							console.log("---file no found----", "$$$$");
							return
						}
						fs.unlink(filepath, (err) => {
							//IF FILE NOT EXIST
							if (err) {
								res.status(400).json(err)
							}
							if (imgIndex + 1 == images.length) {
								console.log("ALL FILES REMOVED---", "$$$$");
								deletePostModel()
							}
						})
					})
				})
			} else {
				deletePostModel()
			} //end if images length
		});

		function deletePostModel() {
			console.log("REMOVING POST MODEL=============", "$$$$");
			Model.Post.remove({
				_id: id
			}, function(err, result) {
				if (err) {
					res.status(err.status).json(err)
				}
				if (result) {
					console.log("POST MODEL DELETED==========", "$$$$");
					res.json(result)
				}
			});
		}
	}
}) //end /delete

let graph = require('../elasticGraph.json').graph
router.get('/search', (req, res, next) => {
	let q = req.query.q
	console.log(q, "q=====$$$$");
	let wordsArray = []
	let keywords = q.split(" ").filter((key) => {
		if (!['in', 'to', 'into', 'for', 'while', 'from'].includes(key)) {
			return key
		}
	})

	console.log(keywords, "$$$$");

	keywords.forEach((word, wordIndex) => {
		graph.forEach((relation, relationIndex) => {
			if (relation.tags.includes(word) || relation.tags.toString().toLowerCase().match(word)) {
				wordsArray = wordsArray.concat(relation.tags)
				if (wordIndex + 1 == keywords.length) {
					wordsArray = Array.from(new Set(wordsArray))
					wordsArray = wordsArray.join(" ")
				}
			}
		})
	})

	q = `${q} | ${wordsArray}`
	elasticClient.search({
		index: 'katiparxa',
		body: {
			min_score: 0.25,
			query: {
				match: {
					_all: q
				}
			}
		}
	}, (error, response) => {
		if (error) res.status(err.status).json(err)
		res.json(response)
	})
})

/*//searching api.....
app.get('/search', async (req,res,next)=>{

	const q = req.query.q

	console.log(q, "q--------$$$$");

	const response = await client.search({
		index: 'lands',
		q:q
	})

	res.json(response.hits)
})*/


router.get('/search2', (req, res, next) => {

	let q = req.query.q
	elasticClient.search({
		index: 'external',
		body: {
			min_score: 0.5,
			from: 0,
			size: 50,
			query: {
				match: {
					_all: q
				}
			}
		}
	}, (error, response) => {
		res.json(response.hits)
	})

})



var Jimp = require("jimp");
router.get('/img-resize/:file/:width?', (req, res, next) => {
	var width = parseInt(req.params.width) || 200;
	if (width > 800) {
		width = 800
	};
	let fileName = "public/images/" + req.params.file
	fs.access(fileName, (err) => {
		if (err) {
			res.status(404).send("file not found!!!")
			return
		}

		Jimp.read(fileName, (err, file) => {
			if (err) {
				console.log(err, "errror....====$$$$");
				throw err
			}

			//if unsupported MIME type
			if (file == undefined) {
				fs.readFile(fileName, (err, data) => {
					res.writeHead(200, {
						'Content-Type': 'image/jpeg'
					});
					res.end(data, 'binary');
				})
				// return;
				return;
			}


			if (file.bitmap.width > 600) {
				//SOME CONDITION FOR THIS
				file.resize(width, Jimp.AUTO).quality(70).getBuffer(Jimp.MIME_JPEG, (err, file) => {
					res.writeHead(200, {
						'Content-Type': 'image/jpeg'
					});
					res.end(file, 'binary'); // Outputting image
				});
			} else {
				fs.readFile(fileName, (err, data) => {
					res.writeHead(200, {
						'Content-Type': 'image/jpeg'
					});
					res.end(data, 'binary');
				})
			}
		});

	})
});

router.get('/user', function(req, res, next) {

	let user = req.user
	Model.User.findOne({
		openID: user.sub
	}, function(err, result) {

		if (err) {
			throw err;
		}
		if (!result) {
			let newUser = {
				name: req.user.name,
				email: req.user.email,
				picture: req.user.picture,
				openID: user.sub
			}
			var userModel = new Model.User(newUser);
			userModel.save(function(err, savedResult) {
				if (err) {
					throw err
				}
				res.json({
					user: savedResult,
					newUser: true
				})
			});
		} else {
			res.json({
				user: result,
				newUser: false
			})
		}
	});

});

router.get('/profile', (req, res, next) => {
	Model.User.findOne({
		_id: req.query.id
	}, function(err, user) {
		if (err) {
			throw err;
		}
		res.json(user);
	});
})

router.get('/users', function(req, res, next) {

	Model.User.find({}, function(err, results) {
		if (err) {
			res.status(400).json(err)
		}
		res.json(results)
	});

})

router.post('/admin/categories', (req, res, next) => {
	let category = req.body
	if (!category.name) {
		res.status(400).json({
			error: "no category name"
		});
		return;
	}

	var model = new Model.Category(category);
	model.save(function(err, category) {
		if (err) {
			res.status(400).json(err)
		}
		res.json(category);
	});
})


router.get('/posts', (req, res, next) => {
	let profile = req.user
	// let openID = profile.sub

	Model.Post.find({}).sort({
		createdAt: -1
	}).exec(function(err, result) {
		if (err) {
			res.status(400).json(err)
		}
		res.json(result)
	});
})




//IF you want to USE socket io then here.....
module.exports = (io) => {
	//*********ELASTIC SEARCH API *************
	router.get('/elastic/actions/index', (req, res, next) => {

		let indexName = "katiparxa";
		elasticClient.indices.exists({
			index: indexName
		}, (err, exist) => {
			if (!exist) {
				console.log("creating index...", "$$$$");
				io.emit('KPMSG', 'creating index...')
				createIndex()
			} else {
				console.log("deleting index....", "$$$$");
				elasticClient.indices.delete({
					index: "katiparxa"
				}, function(err, result) {
					if (err) throw err
					if (result.acknowledged) {
						io.emit('KPMSG', {
							title: "index deleted"
						})
						createIndex()
					}
				});
			}
		});

		function createIndex() {
			elasticClient.indices.create({
				index: indexName,
				body: {
					"settings": {
						"analysis": {
							"analyzer": {
								"my_analyzer": {
									"tokenizer": "my_tokenizer",
									"filter": ["lowercase"]
								}
							},
							"tokenizer": {
								"my_tokenizer": {
									"type": "edge_ngram",
									"min_gram": 2,
									"max_gram": 20,
									"token_chars": [
										"letter",
										"digit"
									]
								}
							}
						}
					},
					"mappings": {
						"posts": {
							"_all": {
								"type": "string",
								"analyzer": "my_analyzer"
							}
						}
					}
				}
			}, function(err, result) {
				if (result.acknowledged) {
					console.log("index created...");
					io.emit('KPMSG', {
						title: 'index created'
					})
					startIndexingAll()
				}
			});
		}


		function startIndexingAll() {
			Model.Post.find({}, function(err, results) {
				if (err) {
					throw err
				}
				results.forEach((result, index) => {
					let body = {
						title: result.title,
						slug: result.slug,
						description: striphtml(result.description),
						userId: result.userId,
						price: result.price,
						brand: result.brand,
						category: result.category,
						vendor: result.vendor,
						images: result.images,
						source: result.source,
						createdAt: result.createdAt,
						updatedAt: result.updatedAt
					}

					elasticClient.index({
						index: 'katiparxa',
						type: "posts",
						id: String(result._id),
						body: body
					}, (err, response) => {
						if (err) throw err
						io.emit('KPMSG', body)

						console.log("DONE--------", body.title, " --$$$$");
						if (index + 1 == results.length) {
							res.json({
								indexed: true,
								size: results.length
							})
						}
					})
				}) //end foreach
			}); //end find
		} //end startIndexingAll()
	}) //end router.get()

	return router;
}


/*----MEDIA----*/
router.post('/media', upload.any(), (req,res,next)=>{

	console.log(req.files, "$$$$");
	if (req.files.length) {
		let images = []
		req.files.forEach(function(file, fileIndex) {
			let fileName = (new Date).valueOf() + '-' + file.originalname
			fs.rename(file.path, `public/images/${fileName}`, (err) => {
				if (err) {
					console.log(err, "$$$$");
					res.status(400).json(err)
				}
				images.push(fileName)
				if (fileIndex + 1 == req.files.length) {
					console.log("all images", "$$$$");
					// res.send("good");
					//create record in database after this...
				}
			})
		});
	} else {
		console.log("ELSE", "$$$$");
		// savePost()
	}
})
