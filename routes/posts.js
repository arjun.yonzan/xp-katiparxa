var express = require('express')
var router = express.Router()
const Model = require('../model')
const slug = require('slug')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.post('/', async (req,res,next)=>{
	/*==================== VALIDATIONS =========================*/
	let post = req.body
	if (!post.title && !post.description) {
		res.status(400).json({
			error: "title | description is empty"
		})
		return
	}; /*-- END VALIDATIONS --*/

	let postSlug = slug(post.title, {lower: true})
	let data = {
		title: post.title,
		slug: postSlug,
		description: post.description,
		price: post.price,
		// brand: post.brand,
		// category: post.category,
		// vendor: post.vendor,
		// images: images || undefined,
		// sourceOfInformation: post.source,
		// openID: openID,
		// user: user._id,
		updatedAt: Date.now()
	}

	try{
		let postModel = new Model.Post(data)
		let result = await postModel.save()
		res.json(result)
	}catch(err){
		// throw err
		res.status(400).send(err)
	}


	/*function savePost(images) {
		let post = req.body

		Model.User.findOne({
			openID: openID
		}, function(err, user) {
			


			postModel.save(function(err, result) {
				if (err) {
					res.status(400).json(err)
				}
				res.json(result)
			}); //end save()
		}); //end findOne
	} //end fn savePost*/

})


module.exports = router;