const ENV = require('./environment')
var mongoose = require('mongoose');
// mongoose.connect(ENV.mongoURL);

/*-------------------------------MONGO CONNECTION----------------------------------------------------------*/
async function connectMongoose(){
	try {
		await mongoose.connect('mongodb+srv://admin:AyBd2020@cluster0-8gyr3.mongodb.net/katiparxa?retryWrites=true&w=majority', {
			useNewUrlParser: true,
			useCreateIndex:true,
			useUnifiedTopology: true
		});
		console.log("ATLAS: mongodb ok", "$$$$");
	} catch (err) {
		console.log(err, " mongoose fail.....$$$$");
	}
}
// connectMongoose()


var User = mongoose.model('User', {
	email: {
		type: String
	},
	name: {
		type: String
	},
	picture:{
		type:String
	},
	openID: {
		type: String,
		unique:true
	}
});

let Post = mongoose.model('Post', {
	title: {
		type: String,
		required: true,
		trim: true
	},
	slug: {
		type:String,
		unique: true,
		required:true,
		trim:true
	},
	description: {
		type: String,
		required: true,
		trim:true
	},
	price: {
		type: String
	},
	vendor: {
		type: String
	},
	brand: {
		type: String
	},
	images: {
		type: Array
	},
	updatedAt:{
		type: Date,
		default: Date.now()
	},
	createdAt: {
		type: Date,
		default: Date.now()
	},
	sourceOfInformation: {
		type: String
	},
	category:String,
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	},
	categories: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: "Category"
	}]
});


/*==================== CATEGORIES MODEL =========================*/
var Category = mongoose.model('Category', {
	name: {
		type: String,
		unique:true,
		trim:true,
		lowercase:true
	}
});

/*==================== BRAND MODEL =========================*/
var Brand = mongoose.model('Brand', {
	title: {
		type: String,
		lowercase:true,
		trim:true
	},
	createdAt: {
		type: Date,
		default: Date.now()
	},
	updatedAt:{
		type:Date,
		default: Date.now()
	}
});

/*==================== VENDOR MODEL =========================*/
var Vendor = mongoose.model('Vendor', {
	title: {
		type: String
	},
	contact: String,
	location:String,
	map: {
		lat: String,
		lng: String
	},
	links:{
		url: String,
		fb:String
	}
});

module.exports = {User, Post, Category}
